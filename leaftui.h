#ifndef LEAFTUI_HEADER
#define LEADTUI_HEADER

#include "vt_commands.h"
#include <conio.h>


#define LEAFTUI_BOARDER_ON  1
#define LEAFTUI_BOARDER_OFF 0

#define LEAFTUI_SHADOW_ON   1
#define LEAFTUI_SHADOW_OFF  0

/* color typedef */
typedef int Color ;

/* main window structure */
typedef struct {
    int x_pos , y_pos , width , height ;
    int reference_x_pos , reference_y_pos ;
    int padding_top, padding_right , padding_bottom , padding_left ;

    int brightness , boarder_brightness , shadow_brightness ;
    int boarder , shadow ;

    Color background , foreground ;
    Color boarder_background , boarder_foreground ;
    Color shadow_background , shadow_foreground ;

} Window ;


typedef struct {
    Window wrapper , content , prompt_space ;
} Textbox ;


typedef struct {
    Window default_look , active_look ;
    int active ;
} Button ;


/* set terminal up to work with the VT100 commands */
int     leaftui_init ( void ) ;
int     leaftui_exit ( void ) ;

/* VT100 esc sequence function short hand */
void    leaftui_reset_attribute ( void ) ;
void    leaftui_hide_cursor     ( void ) ;
void    leaftui_show_cursor     ( void ) ;

/* Window initialization, to create a window element */
Window  leaftui_init_window ( int x_pos , int y_pos , int width , int height , Color background , Color foreground , int brightness ) ;

/* controls to set or charge parts of the window*/
void    leaftui_set_window_position             ( Window *container , int x_pos , int y_pos ) ;
void    leaftui_set_window_size                 ( Window *container , int width , int height ) ;
void    leaftui_set_window_reference_point      ( Window *container , int reference_x_pos , int reference_y_pos ) ;
void    leaftui_set_window_parrent              ( Window *container , Window parrent ) ;
void    leaftui_set_window_padding              ( Window *container , int top , int right , int bottom , int left ) ;
void    leaftui_set_window_padding_xy           ( Window *container , int x_padding , int y_padding ) ;
void    leaftui_set_window_padding_equal        ( Window *container , int padding ) ;
void    leaftui_set_window_background           ( Window *container , Color background ) ;
void    leaftui_set_window_foreground           ( Window *container , Color foreground ) ;
void    leaftui_set_window_colors               ( Window *container , Color background , Color foreground ) ;
void    leaftui_set_window_brightness           ( Window *container , int brightness ) ;
void    leaftui_set_window_colors_full          ( Window *container , Color background , Color foreground , int brightness ) ;
void    leaftui_set_window_shadow               ( Window *container , int shadow_bool ) ;
void    leaftui_set_window_shadow_background    ( Window *container , Color background ) ;
void    leaftui_set_window_shadow_foreground    ( Window *container , Color foreground ) ;
void    leaftui_set_window_shadow_colors        ( Window *container , Color background , Color foreground ) ;
void    leaftui_set_window_shadow_brightness    ( Window *container , int brightness ) ;
void    leaftui_set_window_shadow_colors_full   ( Window *container , Color background , Color foreground , int brightness ) ;
void    leaftui_set_window_boarder              ( Window *container , int boarder_bool ) ;
void    leaftui_set_window_boarder_background   ( Window *container , Color background ) ;
void    leaftui_set_window_boarder_foreground   ( Window *container , Color foreground ) ;
void    leaftui_set_window_boarder_colors       ( Window *container , Color background , Color foreground ) ;
void    leaftui_set_window_boarder_brightness   ( Window *container , int brightness ) ;
void    leaftui_set_window_boarder_colors_full  ( Window *container , Color background , Color foreground , int brightness ) ;



/* drawing windows to the terminal */
int     leaftui_draw_window_boarder ( Window container ) ;
int     leaftui_draw_window_raw     ( Window container , char default_char ) ;
int     leaftui_draw_window_shadow  ( Window container , char default_char ) ;
int     leaftui_draw_window_noshadow( Window container ) ;
int     leaftui_draw_window         ( Window container ) ;


/* draws text to a window */
int     leaftui_put_text_raw            ( Window container , char *text , Color background , Color foreground , int brightness ) ;
int     leaftui_put_text_set_brightness ( Window container , char *text , int brightness ) ;
int     leaftui_put_text_set_color      ( Window container , char *text , Color foreground ) ;
int     leaftui_put_text_custom         ( Window container , char *text , Color foreground , int brightness ) ;
int     leaftui_put_text                ( Window container , char *text ) ;

char*   leaftui_get_text_raw ( int max_len ) ;


#endif
