u#include "leaftui.h"


/*
automated enviorment init and exit
*/
int
leaftui_init ( void )
{
    /*
    allow VT100 esc sequences to be usable and allow terminal to see more
    charactrs.
    save cursor position for restore at leaftui_exit.
    */
    vt_enable () ;
    vt_save_cursor () ;

    return 0 ;
}

int
leaftui_exit ( void )
{
    /*
    reset terminal attributes to user defaults
    restore cursor position, previously saved in leaftui_init
    */
    vt_set_attribute ( RESET_ATTRIBUTE ) ;
    vt_restore_cursor () ;

    return 0 ;
}


/*
VT100 esc sequence function shorthand
*/
void
leaftui_reset_attributes ( void )
{
    /* reset all attributes like cursor and colors */
    vt_set_attribute ( RESET_ATTRIBUTE ) ;
}

void
leaftui_hide_cursor ( void )
{
    /* hades the cursor */
    vt_set_attribute ( CURSOR_HIDDEN ) ;
}

void
leaftui_show_cursor ( void )
{
    /* sets cursor to blinking */
    vt_set_attribute ( CURSOR_BLINK ) ;
}

void
reset_attributes_and_hide_cursor ( void )
{
    /*
    resets attributes and sets curser to hidden
    for printing things to the screen
    */
    leaftui_reset_attributes () ;
    leaftui_hide_cursor () ;

}


/*
Window Constructor
*/
Window
leaftui_init_window ( int x_pos , int y_pos , int width , int height , Color background , Color foreground , int brightness )
{
    /*
    simple "new Window" function/constructor
    */
    Window new_window ;

    /* position */
    leaftui_set_window_position ( &new_window , x_pos , y_pos ) ;
    /* reference point */
    leaftui_set_window_reference_point ( &new_window , 0 , 0 ) ;

    /* size */
    leaftui_set_window_size     ( &new_window , width , height ) ;
    /* padding */
    leaftui_set_window_padding  ( &new_window , 0 , 0 ) ;

    /* colors */
    leaftui_set_window_colors_full ( &new_window , background , foreground , brightness ) ;

    /* shadow */
    leaftui_set_window_shadow ( &new_window , LEAFTUI_SHADOW_OFF ) ;
    leaftui_set_window_shadow_colors_full ( &new_window , B_BLACK , F_BLACK , BRIGHT_TEXT ) ;

    /* boarder */
    leaftui_set_window_boarder ( &new_window , LEAFTUI_BOARDER_OFF ) ;
    leaftui_set_window_boarder_colors_full ( &new_window , background , foreground , BRIGHT_TEXT ) ;

    return new_window ;
}


/*
Window set functions
*/
void
leaftui_set_window_position ( Window *container , int x_pos , int y_pos )
{
    /*
    function alternitive to changing windows positioning manualy
    alternate option is manually by changing window.x_pos and window.y_pos
    ( note: all possitioning is based on top left corner )
    */
    (*container).x_pos = x_pos ;
    (*container).y_pos = y_pos ;
}

void
leaftui_set_window_size ( Window *container , int width , int height )
{
    /*
    function alternitive to changing windows sizeing manualy
    alternate option is manually by changing window.width and window.height
    */
    (*container).width = width ;
    (*container).height = height ;
}

void
leaftui_set_window_reference_point ( Window *container , int reference_x_pos , int reference_y_pos )
{
    /*
    sets windows reference point from a suplied x and y positioning
    */
    (*container).reference_x_pos = reference_x_pos ;
    (*container).reference_y_pos = reference_y_pos ;
}

void
leaftui_set_window_parrent ( Window *container , Window parrent )
{
    /*
    sets window's reference point from a parrent windows possition
    */
    leaftui_set_window_reference_point ( container , parrent.x_pos , parrent.y_pos ) ;
}

void
leaftui_set_window_padding ( Window *container , int top , int right , int bottom , int left )
{
    /*
    functional alternitive to changing window text padding manualy
    alternate option is manually by changing window.padding_top ,
    window.padding_right , window.padding_bottom , and window.padding_left
    */
    (*container).padding_top = top ;
    (*container).padding_right = right ;
    (*container).padding_bottom = bottom ;
    (*container).padding_left = left ;

}

void
leaftui_set_window_padding_xy ( Window *container , int x_padding , int y_padding )
{
    /*
    sets windows padding for x and y cords
    simplification of standard window padding
    */
    leaftui_set_window_padding ( container , y_padding , x_padding , y_padding , x_padding ) ;
}

void
leaftui_set_window_padding_equal ( Window *container , int padding )
{
    /*
    sets window padding equaly on all sides
    */
    leaftui_set_window_padding ( container , padding , padding , padding , padding ) ;
}

/* window color set commands */
void
leaftui_set_window_background ( Window *container , Color background )
{
    /*
    function alternitive to changing windows background color
    alternate option is manualy by changengi window.background
    */
    (*container).background = background ;
}

void
leaftui_set_window_foreground ( Window *container , Color foreground )
{
    /*
    function alternitive to changing windows foreground color
    alternate option is manually by changing window.foreground
    */
    (*container).foreground = foreground ;
}

void
leaftui_set_window_colors ( Window *container , Color background , Color foreground )
{
    /*
    function alternitive to changing windows colors manualy
    alternate option is manually by changing window.background and
    window.foreground
    */
    leaftui_set_window_background ( container , background ) ;
    leaftui_set_window_foreground ( container , foreground ) ;
}

void
leaftui_set_window_brightness ( Window *container , int brightness )
{
    /*
    function alternitive to changing windows brightness between BRIGHT and DULL
    alternate option is manually by changing window.brightness value
    */
    (*container).brightness = brightness ;
}

void
leaftui_set_window_colors_full ( Window *container , Color background , Color foreground , int brightness )
{
    /*
    function alternitive to changing brightness and colors by hand
    alternate option is manualy changing window.background, window.foreground,
    window.brightness
    */
    leaftui_set_window_colors ( container , background , foreground ) ;
    leaftui_set_window_brightness ( container , brightness ) ;
}

/* shadow set commands */
void
leaftui_set_window_shadow ( Window *container , int shadow_bool )
{
    /*
    function alternitive to changing windows shadow between LEAFTUI_SHADOW_ON
    and LEAFTUI_SHADOW_OFF
    alternate option is manually by changing window.shadow
    */
    (*container).shadow = shadow_bool ;
}

void
leaftui_set_window_shadow_background ( Window *container , Color background )
{
    /*
    function alternitive to changing the window shadow's background color
    alternate option is manually by changing window.shadow_background
    */
    (*container).shadow_background = background ;
}

void
leaftui_set_window_shadow_foreground ( Window *container , Color foreground )
{
    /*
    function alternitive to changing the window shadow's foreground color
    alternate option is manually by changing window.shadow_foreground
    */
    (*container).shadow_foreground = foreground ;
}

void
leaftui_set_window_shadow_colors ( Window *container , Color background , Color foreground )
{
    /*
    function alternitive to changith the windows shadow's colors
    alternitive option is manually by changing both window.shadow_background
    and window.shadow_foreground
    */
    leaftui_set_window_shadow_background ( container , background ) ;
    leaftui_set_window_shadow_foreground ( container , foreground ) ;
}

void
leaftui_set_window_shadow_brightness ( Window *container , int brightness )
{
    /*
    function alternitive to changing windows brightness between BRIGHT and DULL
    alternate option is manually by changing window.brightness value
    */
    (*container).shadow_brightness = brightness ;
}

void
leaftui_set_window_shadow_colors_full ( Window *container , Color background , Color foreground , int brightness )
{
    /*
    function alternitive to changing brightness and colors by hand
    alternate option is manualy changing window.background, window.foreground,
    window.brightness
    */
    leaftui_set_window_shadow_colors ( container , background , foreground ) ;
    leaftui_set_window_shadow_brightness ( container , brightness ) ;
}

/* boarder set commands */
void
leaftui_set_window_boarder ( Window *container , int boarder_bool )
{
    /*
    function alternitive to changing window's boarder between LEAFTUI_BOARDER_ON
    and LEAFTUI_BOARDER_OFF
    alternate option is manually by changing window.boarder
    */
    (*container).boarder = boarder_bool ;
}

void
leaftui_set_window_boarder_background ( Window *container , Color background )
{
    /*
    function alternitive to changing the window boarder's background color
    alternate option is manually by changing window.boarder_background
    */
    (*container).boarder_background = background ;
}

void
leaftui_set_window_boarder_foreground ( Window *container , Color foreground )
{
    /*
    function alternitive to changing the window boarder's foreground color
    alternate option is manually by changing window.boarder_foreground
    */
    (*container).boarder_foreground = foreground ;
}

void
leaftui_set_window_boarder_colors ( Window *container , Color background , Color foreground )
{
    /*
    function alternitive to changing the window shadow's colors
    alternate option is manually by changing window.shadow_background and
    window.shadow_foreground
    */
    leaftui_set_window_boarder_background ( container , background ) ;
    leaftui_set_window_boarder_foreground ( container , foreground ) ;
}

void
leaftui_set_window_boarder_brightness ( Window *container , int brightness )
{
    /*
    function alternitive to changing windows brightness between BRIGHT and DULL
    alternate option is manually by changing window.boarder_brightness value
    */
    (*container).boarder_brightness = brightness ;
}

void
leaftui_set_window_boarder_colors_full ( Window *container , Color background , Color foreground , int brightness )
{
    /*
    function alternitive to changing the boarder brightness and colors by hand
    alternate option is manualy changing window.boarder_background,
    window.boarder_foreground, and window.boarder_brightness
    */
    leaftui_set_window_boarder_colors ( container , background , foreground ) ;
    leaftui_set_window_boarder_brightness ( container , brightness ) ;
}


/*
Functions working with drawing window elements
*/
int
leaftui_draw_window_raw ( Window container , char default_char )
{
    /*
    Simple draw function to draw window as a blank color
    no shadow, no boarder, just a plain blank window
    */
    int x_iter , y_iter ;

    /* set only attribute as hidden cursor */
    reset_attributes_and_hide_cursor() ;
    /* set window colors and brightness */
    vt_set_attribute ( container.background ) ;
    vt_set_attribute ( container.foreground ) ;
    vt_set_attribute ( container.brightness ) ;
    /* hove cursor to top right corner */
    vt_move_cursor ( container.x_pos , container.y_pos ) ;
    /* simple 2D for loop */
    for ( y_iter = container.height - 1 ; y_iter >= 0 ; y_iter-- )
    {
        for ( x_iter = container.width - 1 ; x_iter >= 0 ; x_iter-- )
        {
            /* draw default character */
            putchar ( default_char ) ;
        }
        /* move curoser to nextline "\n\r" */
        vt_move_cursor ( container.x_pos , ( container.y_pos + y_iter ) ) ;
    }

    return 0 ;
}

int
leaftui_draw_window_shadow ( Window container , char default_char )
{
    /*
    draws a window shadow
    */

    /* offsets shadow to window */
    container.x_pos++ ;
    container.y_pos++ ;

    /* set shadow colors */
    leaftui_set_window_colors_full ( &container , container.shadow_background , container.shadow_foreground , container.shadow_brightness ) ;

    /* draw shadow */
    return leaftui_draw_window_raw ( container , default_char ) ;
}

int
leaftui_draw_window_noshadow ( Window container )
{
    /*
    draws a window with no shadow despite what the window.shadow property
    says
    */
    int exitcode ;

    /* draw the base window */
    exitcode = leaftui_draw_window_raw ( container , ' ' ) ;
    /* if draw window has an issue return without drawing the rest of the window */
    if (exitcode != 0 )
        return exitcode ;

    /* if the window has a boarder draw it */
    if ( container.boarder == LEAFTUI_BOARDER_ON )
        exitcode = leaftui_draw_window_boarder ( container ) ;

    return exitcode ;
}

int
leaftui_draw_window ( Window container )
{
    /*
    reads window settings and draws elements accordingly.
    if shadow == on then itll draw shadow. ect.
    */
    int exitcode ;

    /* draw shadow if needed */
	if ( container.shadow == LEAFTUI_SHADOW_ON )
		exitcode = leaftui_draw_window_shadow ( container , ' ' ) ;
    /* if draw shadow has an issue, return without drawing rest of screen */
    if ( exitcode != 0 )
        return exitcode ;

    /* draw the rest of the window */
    return leaftui_draw_window_noshadow ( container ) ;
}

int
leaftui_draw_window_boarder ( Window container )
{
    char boarder_corner_top_left , boarder_corner_top_right , boarder_corner_bottom_right , boarder_corner_bottom_left , boarder_horizontal , boarder_vertical ;
    int x_pos , y_pos ;

    boarder_corner_top_left = '.' ;
    boarder_corner_top_right = '.' ;
    boarder_corner_bottom_right = '\'' ;
    boarder_corner_bottom_left = '`' ;
    boarder_horizontal = '-' ;
    boarder_vertical = '|' ;

    /* set only attribute as hidden cursor */
    reset_attributes_and_hide_cursor () ;

    vt_set_attribute ( container.boarder_brightness ) ;
    vt_set_attribute ( container.boarder_background ) ;
    vt_set_attribute ( container.boarder_foreground ) ;

    vt_move_cursor ( container.x_pos , container.y_pos ) ;
    putchar ( boarder_corner_top_left ) ;

    vt_move_cursor ( container.x_pos + container.width - 1 , container.y_pos ) ;
    putchar ( boarder_corner_top_right ) ;

    vt_move_cursor ( container.x_pos + container.width - 1 , container.y_pos + container.height - 1) ;
    putchar ( boarder_corner_bottom_right ) ;

    vt_move_cursor ( container.x_pos , container.y_pos + container.height - 1 ) ;
    putchar ( boarder_corner_bottom_left ) ;


    for ( x_pos = container.x_pos + 1 ; x_pos <= container.x_pos + container.width - 2 ; x_pos++ )
    {
        vt_move_cursor ( x_pos , container.y_pos  ) ;
        putchar ( boarder_horizontal ) ;

        vt_move_cursor ( x_pos , container.y_pos + container.height - 1 ) ;
        putchar ( boarder_horizontal ) ;
    }

    for ( y_pos = container.y_pos + 1 ; y_pos <= container.y_pos + container.height - 2 ; y_pos++ )
    {
        vt_move_cursor ( container.x_pos , y_pos ) ;
        putchar ( boarder_vertical ) ;

        vt_move_cursor ( container.x_pos + container.width - 1 , y_pos ) ;
        putchar ( boarder_vertical ) ;
    }

    return 0 ;
}


/*
Functions for writing text to windows and controlling the behavior of how its
being written
*/
int
leaftui_put_text_raw ( Window container , char *text , Color background , Color foreground , int brightness )
{
    /*
    draws text to a window, acconuting for padding and wrapping text when
    needed.
    in use you need to specify the window , the text , the brightness, and
    the colors
    Is used mainly as a base function for the following bellow
    */
    int current_row , current_col , max_char_count , i ;
    char ch ;

    /* set control variables */
    max_char_count = ( ( ( container.width - container.padding_right - container.padding_left ) ) * ( ( container.height - container.padding_top - container.padding_bottom ) ) ) ;
    current_row = 0 + container.padding_top ;
    current_col = 0 + container.padding_right ;

    i = 0 ;

    /* set only attribute as hidden cursor */
    reset_attributes_and_hide_cursor () ;

    /* set colors and brightness */
    vt_set_attribute ( brightness ) ;
    vt_set_attribute ( background ) ;
    vt_set_attribute ( foreground ) ;
    /* move cursor to possition */
    vt_move_cursor ( ( container.x_pos + container.text_padding_x ) , ( container.y_pos + container.text_padding_y ) ) ;
    /* drawing while loop */
    while ( i < max_char_count && text[i] != '\0' && current_row <= ( container.height - container.text_padding_y - 1 ) )
    {
        /* get next char in text */
        ch = text[i++] ;

        /*
        if character is a newline or reaches side of the window wrap to a new
        line
        */
        if ( ch == '\n' || current_col > ( container.width - container.text_padding_x - 1 ) )
        {
            current_col = 0 + container.text_padding_x ;
            current_row++ ;
            vt_move_cursor ( ( container.x_pos + container.text_padding_x ) , ( container.y_pos + current_row ) ) ;

            while ( ch == ' ' )
            {
                ch = text[i++] ;
            }
        }

        if ( ch != '\n' )
        {
            /* draw character */
            putchar ( ch ) ;
            current_col++ ;
        }
    }

    return 0 ;
}

int
leaftui_put_text ( Window container , char *text )
{
    /*
    simple draw text, uses window's setting to write text to the screen
    */
    return leaftui_put_text_raw ( container , text , container.background , container.foreground , container.brightness ) ;
}

int
leaftui_put_text_set_color ( Window container , char *text , Color foreground )
{
    /*
    draws text to window specifying a text color
    uses window's brightness and background color
    */
    return leaftui_put_text_raw ( container , text , container.background , foreground , container.brightness ) ;
}

int
leaftui_put_text_set_brightness ( Window container , char *text , int brightness )
{
    /*
    draws text to window specifying a text brightness
    uses window's colors.
    */
    return leaftui_put_text_raw ( container , text , container.background , container.foreground , brightness ) ;
}

int
leaftui_put_text_custom ( Window container , char *text , Color foreground , int brightness )
{
    /*
    draws text to window specifying text color and brightness
    uses window's background color.
    */
    return leaftui_put_text_raw ( container , text , container.background , foreground , brightness ) ;
}


char*
leaftui_get_text_of_length ( int max_len )
{
    /*
    gets single line text input of given length
    with arrow key and backspace functionality
    */

    char    ch , *input_str ;
    int     i ;

    /* base values */
    ch = '\0' ;
    i = 0 ;

    /* clear and allocate the return string */
    input_str = (char*)calloc ( sizeof (char) , max_len ) ;

    /* loop till a newline is sent */
    while ( ( ch != '\n' && ch != '\r' && ch != '\t' ) || i == 0 )
    {
        /* get char */
        ch = getch () ;

        /* determines what to do by what character was sent */
        switch ( ch )
        {
        /* backspace = remove 1 char and go back */
        case '\b' :
            if ( i <= 0 )
                break ;
            input_str[--i] = '\0' ;
            vt_move_cursor_left ( 1 ) ;
            putchar ( ' ' ) ;
            vt_move_cursor_left ( 1 ) ;
            break ;

        /* arrow keys */
        case '�' :
            /* get next character */
            ch = getch () ;

            /* determine what to do next based on the arrow key that was pressed */
            switch ( ch )
            {
            /* left arrow */
            case 'K' :
                if ( i <= 0 )
                    break ;
                vt_move_cursor_left ( 1 ) ;
                i-- ;
                break ;

            /* down arrow = home */
            case 'P' :
                if ( i <= 0 )
                    break ;
                vt_move_cursor_left ( i ) ;
                i = 0 ;
                break ;

            /* up arrow = end */
            case 'H' :
                if ( i >= max_len - 1 )
                    break ;
                vt_move_cursor_right ( max_len - i - 1 ) ;
                i = max_len - 1 ;
                break ;

            /* right arrow */
            case 'M' :
                if ( i >= max_len - 1 )
                    break ;
                vt_move_cursor_right ( 1 ) ;
                i++ ;
                break ;

            }
            break ;

        /* stinki characters we dont want */
        case '\r' :
        case '\n' :
        case '\t' :
            break ;

        /* any character not picked up from the above */
        default :
            if ( i >= max_len - 1 )
                break ;
            /* add character to the list, while incrementing i */
            input_str[i++] = ch ;
            /* display character */
            putchar ( ch ) ;

            break ;
        }
    }

    /* return the string */
    return input_str ;
}


