#include "vt_commands.h"

int
vt_enable ( void )
{
    HANDLE  h_out , h_in ;
    DWORD   dw_original_out_mode , dw_original_in_mode , dw_requested_out_mode , dw_requested_in_mode , dw_out_mode , dw_in_mode ;

    h_out = GetStdHandle ( STD_OUTPUT_HANDLE ) ;
    if ( h_out == INVALID_HANDLE_VALUE )
    {
        printf ( "h_out_invalid_handle\n" ) ;
        return 1 ;
    }

    h_in = GetStdHandle ( STD_INPUT_HANDLE ) ;
    if ( h_in == INVALID_HANDLE_VALUE )
    {
        printf ( "h_in_invalid_handle\n" ) ;
        return 1 ;
    }

    dw_original_out_mode = 0 ;
    dw_original_in_mode = 0 ;
    if ( !GetConsoleMode ( h_out , &dw_original_out_mode ) )
    {
        printf ( "GetConsoleMode h_out\n" ) ;
        return 1 ;
    }
    if ( !GetConsoleMode ( h_in , &dw_original_in_mode ) )
    {
        printf ( "GetConsoleMode h_in\n" ) ;
        return 1 ;
    }

    dw_requested_out_mode = ENABLE_VIRTUAL_TERMINAL_PROCESSING | DISABLE_NEWLINE_AUTO_RETURN ;
    dw_requested_in_mode = ENABLE_VIRTUAL_TERMINAL_INPUT ;

    dw_out_mode = dw_original_out_mode | dw_requested_out_mode ;
    if ( !SetConsoleMode ( h_out , dw_out_mode ) ) ;
    {
        dw_requested_out_mode = ENABLE_VIRTUAL_TERMINAL_PROCESSING ;
        dw_out_mode = dw_original_out_mode | dw_requested_out_mode ;
        if ( !SetConsoleMode ( h_out , dw_out_mode ) )
        {
            return -1 ;
        }
    }

    dw_in_mode = dw_original_in_mode | dw_requested_out_mode ;
    if ( !SetConsoleMode ( h_in , dw_in_mode ) )
    {
        return -1 ;
    }

    return 0 ;
}


void
vt_enable_more_characters ( void )
{
    SetConsoleOutputCP ( 65001 ) ;
    return ;
}


void
vt_move_cursor ( int x_pos , int y_pos )
{
    printf ( "\x1b[%d;%dH" , y_pos , x_pos ) ;
    return ;
}


void
vt_move_cursor_up ( int y_pos )
{
    printf ( "\x1b[%dA" , y_pos ) ;
    return ;
}


void
vt_move_cursor_down ( int y_pos )
{
    printf ( "\x1b[%dB" , y_pos ) ;
    return ;
}


void
vt_move_cursor_right ( int x_pos )
{
    printf ( "\x1b[%dC" , x_pos ) ;
    return ;
}


void
vt_move_cursor_left ( int x_pos )
{
    printf ( "\x1b[%dD" , x_pos ) ;
    return ;
}


void
vt_clear_screen ( void )
{
    printf ( "\x1b[2J" ) ;
    return ;
}


void
vt_set_attribute ( int code )
{
    printf ( "\x1b[%dm" , code ) ;
    return ;
}

void
vt_save_cursor ( void )
{
    printf ( "\x1b[s" ) ;
    return ;
}

void
vt_restore_cursor ( void )
{
    printf ( "\x1b[u" ) ;
    return ;
}

