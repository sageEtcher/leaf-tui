#ifndef VT_HEADER
#define VT_HEADER

#include <windows.h>
#include <wchar.h>
#include <stdio.h>

/* Defining needed constants for windows.h being its inconsistent */
#ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING  0x0004
#endif

#ifndef ENABLE_VIRTUAL_TERMINAL_INPUT
#define ENABLE_VIRTUAL_TERMINAL_INPUT       0x0200
#endif

#ifndef DISABLE_NEWLINE_AUTO_RETURN
#define DISABLE_NEWLINE_AUTO_RETURN         0x0008
#endif



/*
Attributes

RESET_ATTRIBUTE = resets all attributes to off

NOTE: If BRIGHT_TEXT is on you cannot change to DULL_TEXT without first
clearing attributes with RESET_ATTRIBUTE)

BRIGHT_TEXT   = uses lighter version of foreground color
DULL_TEXT     = uses darker version of foreground color

REVERSE       = Swaps background and foreground colors
UNDERLINE     = Underlines all text printed

CURSOR_BLINK  = show cursor
CURSOR_HIDDEN = hides cursor

NOTE: none of these are togglable with the exception of REVERSE.
to get rid of an attribute you must clear all of them with RESET_ATTRIBUTE.
*/

#define RESET_ATTRIBUTE 0
#define BRIGHT_TEXT     1
#define DULL_TEXT       2
#define UNDERLINE       4
#define CURSOR_BLINK    5
#define REVERSE         7
#define CURSOR_HIDDEN   8


/*
Color Codes

Mix with foreground with BRIGHT_TEXT or DULL_TEXT to get more, background is
limmited to only those 8

F_COLOR = foreground color, ie text
B_COLOR = background color, ie hightlight
*/

#define F_BLACK         30
#define F_RED           31
#define F_GREEN         32
#define F_YELLOW        33
#define F_BLUE          34
#define F_MAGENTA       35
#define F_CYAN          36
#define F_WHITE         37

#define B_BLACK         40
#define B_RED           41
#define B_GREEN         42
#define B_YELLOW        43
#define B_BLUE          44
#define B_MAGENTA       45
#define B_CYAN          46
#define B_WHITE         47


/* vt100 functions */
int     vt_enable ( void ) ;
void    vt_enable_more_characters ( void ) ;
void    vt_move_cursor ( int x_pos , int y_pos ) ;
void    vt_move_cursor_up ( int y_pos ) ;
void    vt_move_cursor_down ( int y_pos ) ;
void    vt_move_cursor_right ( int x_pos ) ;
void    vt_move_cursor_left ( int x_pos ) ;
void    vt_clear_screen ( void ) ;
void    vt_set_attribute ( int code ) ;
void    vt_save_cursor ( void ) ;
void    vt_restore_cursor ( void ) ;


#endif
