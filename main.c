#include "leaftui.h"


/* pages */
char*   ask_user_for_name       ( Window header , Window body , Window footer ) ;
void    welcome_user_by_name    ( Window body , Window footer , char *name ) ;


/* program entry point */
int
main (  )
{
    Window  wrapper , header , body , footer ;
    char    *name ;

    /* initiate window objects */
    wrapper   = leaftui_init_window ( 0,0   , 80,25 , B_WHITE   , F_BLACK , DULL_TEXT ) ;
    header    = leaftui_init_window ( 0,0   , 80,1  , B_BLACK   , F_WHITE , DULL_TEXT ) ;
    body      = leaftui_init_window ( 8,5   , 64,16 , B_MAGENTA , F_WHITE , BRIGHT_TEXT ) ;
    footer    = leaftui_init_window ( 0,25  , 80,1  , B_BLACK   , F_WHITE , DULL_TEXT ) ;

    /* style the body */
    leaftui_set_window_padding ( &body , 3 , 2 ) ;
    leaftui_set_window_shadow  ( &body , LEAFTUI_SHADOW_ON ) ;
    leaftui_set_window_shadow_background ( &body , B_BLACK ) ;
    leaftui_set_window_boarder ( &body , LEAFTUI_BOARDER_ON ) ;
    leaftui_set_window_boarder_foreground ( &body , F_WHITE ) ;

    /* enable vt100 esc sequences */
    leaftui_init () ;

    /* draw items to screen */
    leaftui_draw_window ( wrapper ) ;

    name = ask_user_for_name ( header , body , footer ) ;
    welcome_user_by_name ( body , footer , name ) ;

    /* exit leaftui */
    leaftui_exit ();
    /* move to next line */
    vt_move_cursor ( 0, 26 ) ;
    /* free pointers */
    free ( name ) ;

    /* exit program */
    return 0 ;
}


/* pages definitions */
char*
ask_user_for_name ( Window header , Window body , Window footer )
{
    /*
        page 1
        draw elements and prompt user for their name
    */
    Window textbox ;
    char *name ;

    /* create the textbox */
    textbox   = leaftui_init_window ( 15,16 , 32,3  , B_WHITE , F_BLACK  , DULL_TEXT ) ;

    /* style the textbox */
    leaftui_set_window_padding ( &textbox , 1 , 1 ) ;
    leaftui_set_window_boarder ( &textbox , LEAFTUI_BOARDER_ON ) ;
    leaftui_set_window_boarder_foreground ( &textbox , F_BLACK ) ;

    /* draw elements */
    leaftui_draw_window ( header ) ;
    leaftui_draw_window ( body ) ;
    leaftui_draw_window ( footer ) ;
    leaftui_draw_window ( textbox ) ;
    /* input text */
    leaftui_put_text ( header , "\tAsk User's Name 9001" ) ;
    leaftui_put_text ( body   , "Welcome to this test application, it doesn\'t do anything asside from asking your name.\nOh! And a quick note, the name is limitted to 16 character and you can only input letters. Backspace should work, though arrows wont. \nAnd yeah, that should be everything, goodluck breaking it! let me know how you did manage to break it, if you do!" ) ;
    leaftui_put_text ( footer , "Please input your name." ) ;
    /* prompt user */
    leaftui_put_text ( textbox , "My name is: " ) ;
    name = leaftui_get_text_raw ( 18 ) ;

    /* return the name */
    return name ;
}


void
welcome_user_by_name ( Window body , Window footer , char *name )
{
    /*
        page 2
        draw redraw some elements and welcome the user by name
    */
    char *new_msg ;

    /* generate page's body contents */
    new_msg = (char*)calloc ( sizeof (char) , 100 ) ;
    snprintf ( new_msg , 100 , "Well hello there, %s, what brings you to these parts?" , name ) ;   \

    /* redraw body and footer */
    leaftui_draw_window_noshadow ( body ) ;
    leaftui_draw_window_noshadow ( footer ) ;
    /* print text */
    leaftui_put_text ( body , new_msg ) ;
    leaftui_put_text ( footer , "Program finished. Exiting . . . " ) ;

    /* free the allocated memory */
    free ( new_msg ) ;

    /* return */
    return ;
}

